var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var db = mongoose.connect('mongodb://localhost/myFirstDb');

var usersSchema = mongoose.Schema({
    nom: String,
    prenom: String
});

const UserModel = mongoose.model("User", usersSchema, 'user');

/* GET users listing. */
router.get('/', async function(req, res, next) {
    await UserModel.find(function(err, users) {
        if (err)
            res.status(400).send(err);
        console.log(users);
        res.send(users);
    });
});

router.get('/:id', async function(req, res, next) {
    await UserModel.findById(req.params.id, function(err, zuser) {
        if (err)
            res.status(400).send(err);
        if (zuser == null)
            res.status(404).send('User not found');

        res.send(zuser);
    });
});

router.post('/', async function(req, res, next) {
    try {
        var User = new UserModel(req.body);
        var result = await User.save();
        res.send(result);
    } catch (error) {
        res.status(500).send(error);
    }
});

router.put('/:id', async function(req, res, next) {
    try {
        var user = await UserModel.findById(req.params.id);
        user.set(req.body);
        var result = await user.save();
        res.send(result);
    } catch (error) {
        res.status(500).send(error);
    }
});

router.delete('/:id', async function(req, res, next) {
    await UserModel.deleteOne({ _id: req.params.id }, function(err, result) {
        if (err)
            res.status(500).send(err);

        res.send(result);
    })
});

module.exports = router;