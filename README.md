# Une API en NodeJS couplée à MongoDb

## Génération du projet avec express 
Dans un terminal (power shell sous windows) ou terminal de votre choix sous Linux
`express nodejs-api-mongo`

Si Windows vous met un message d'erreur lors d'exécution d'express, exécutez la commande suivante : 
`Set-ExecutionPolicy -ExecutionPolicy Bypass -Scope CurrentUser` qui permet à l'utilisateur courant d'exécuter des scripts non signés. 

## Routes pour l'API
Toutes les routes de l'API pour répondre aux différentes requêtes comme GET, POST, PUT, DELETE, ont été ajoutées à `routes/users.js`

## Connexion à la base de données MongoDb
La base de données MongoDb doit être installée et être lancée en tant que service sur votre poste. 

Pour avoir un **connector** qui se lit à notre base de données, nous avons installé un module avec la commande : 
`npm install mongoose`

Notre base de données est appelée grâce à la ligne : `var db = mongoose.connect('mongodb://localhost/myFirstDb');`où myFirstDb est la base de données sur laquelle se trouve nos données de tests. 

## supervisor pour rechargement à chaud
Si vous souhaitez avoir un rechargement à chaud, lorsque vous développez votre API, installez le module **nodemon** en global avec la commande suivante : 
`npm install -g nodemon`

Maintenant vous pouvez lancer votre application avec la commande `nodemon ./bin/www` plutôt que `npm start`.

## Exécution de l'API
Pour tester l'application, lancez postman et exécutez vos requêtes sur http://localhost:3000. 

### POST sur users
Toujours avec postman, faite une requête **POST** sur l'url http://localhost:3000/users en remplissant votre body avec le json au format suivant : 
```
{
    "nom": "Duchamp",
    "prenom": "Bruce"
}
```
Répétez l'opération plusieurs fois avec des valeurs différentes de manière à avoir plusieurs utilisateurs dans votre base. 

### GET sur users
Un *GET* sur la même url, vous donnera un tableau de json contenant vos utilisateurs. 

### GET d'un users
Dans postman, rentrez l'url avec l'id d'un utilisateur existant en opérant un GET sur une url du type `http://localhost:/users/5fdb49517900276bc49faa1e`
Un json contenant les informations de l'utilisateur doit arriver dans la fenêtre de résultat. 

